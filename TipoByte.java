/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package lesm.tamanioAlmacenamiento;

/**
 *
 * @author Luis
 */
public class TipoByte{      
    
    
    
    public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
    }
    
    
    public String sumaDeByte(String numByteUno, String numByteDos){
        String resultado ="los número no son los corretos";
        System.out.println("Sumando dos numeros Bytes");
        if(numByteUno.length() <= 8 && numByteDos.length() <=8){            
           resultado = sumaDeBinarios(numByteUno, numByteDos);
           if(resultado.length() > 8){
               resultado = "Desvordamiento de pila----->>  00000000";
            }
        
        }
        return resultado;
    }
    public String sumaDeByteWord(String numByte, String numWord){
        String resultado ="los número no son los corretos";
        System.out.println("Sumando un número Byte mas un Word");
        if(numByte.length() <= 8 && numWord.length() <=16){            
           resultado =  sumaDeBinarios(numByte,numWord);
           if(resultado.length() > 16){
               resultado = "Desvordamiento de pila----->>  0000000000000000";
            }
       
    }
        return resultado;
    }
    
      public String sumaDeByteDWord(String numByte, String numDWord){
        String resultado ="los número no son los corretos";
        System.out.println("Sumando un número Byte mas un DWord");
        if(numByte.length() <= 8 && numDWord.length() <=32){            
           resultado =  sumaDeBinarios(numByte,numDWord);
           if(resultado.length() > 32){
               resultado = "Desvordamiento de pila----->>  00000000000000000000000000000000";
            }
       
    }
        return resultado;
    }
      
      public String sumaDeByteQWord(String numByte, String numQWord){
        String resultado ="los número no son los corretos";
        System.out.println("Sumando un número Byte mas un QWord");
        if(numByte.length() <= 8 && numQWord.length() <=64){            
           resultado =  sumaDeBinarios(numByte,numQWord);
           if(resultado.length() > 64){
               resultado = "Desvordamiento de pila----->>  00000000000000000000000000000000000000000000000000000000000000000000000000000000";
            }
       
    }
        return resultado;
    }
    
    

public static void main(String[] agrs){
    TipoByte uno = new TipoByte();
    System.out.println(uno.sumaDeByte("110","11"));
    System.out.println(uno.sumaDeByteWord("10101", "1001010101"));
    System.out.println(uno.sumaDeByteDWord("1110","101010010010111011"));
    System.out.println(uno.sumaDeByteQWord("10010","1001010010101010001100101011"));
}
}
